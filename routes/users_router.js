const express = require('express');
const UserRouter = express.Router();
const UserModel = require('../models/User')
const bcrypt = require('bcryptjs');


UserRouter.post('/register', async(req, res)=>{
  if(!req.body.email) return res.status(400).send('Email is required');
  if(!req.body.password) return res.status(400).send('Password is required');

  let user = UserModel({
    name: req.body.name,
    email: req.body.email
  });

  let salt = bcrypt.genSaltSync(10);
  let hashed = bcrypt.hashSync(req.body.password, salt);
  user.password = hashed;

  try{
    user = await user.save();
    res.send(user)
  }catch(e){
    res.status(400).send("Invalid Data");
    console.log(e)
  }
})

module.exports = UserRouter;