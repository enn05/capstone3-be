const express = require('express');

const InstructorRoute = express.Router();

const InstructorModel = require('../models/Instructor');

InstructorRoute.post('/addinstructor', async(req, res)=>{
  try{
    let newInstructor = InstructorModel({
      name: req.body.name,
      email: req.body.email,
      genre: req.body.genre
    })

    newInstructor = await newInstructor.save();
    res.send(newInstructor);
  }catch(e){
    console.log(e);
  }
})

InstructorRoute.get('/showinstructors', async(req, res)=>{
  try{
    let instructors = await InstructorModel.find();
    res.send(instructors);
  }catch(e){
    console.log(e)
  }
})

InstructorRoute.delete('/deleteinstructor/:id', async(req, res)=>{
  try{
    let deleteInstructor = await InstructorModel.findByIdAndDelete(req.params.id)
    res.send(deleteInstructor)
  }catch(e){
    console.log(e)
  }
})

module.exports = InstructorRoute;