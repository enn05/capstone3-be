const express = require('express');
const BookingRouter = express.Router();
const BookingModel = require('../models/Booking');

BookingRouter.post('/addbooking', async (req, res)=>{
	try{
		let booking = BookingModel({
			userName:req.body.userName,
    	userId:req.body.userId,
      className: req.body.className,
      classId: req.body.classId,
			dates: req.body.dates,
			bookingNumber: req.body.bookingNumber,
			totalPayment:req.body.totalPayment,
			status: req.body.status,
			transactionCode: req.body.transactionCode
		});
		booking = await booking.save();
		res.send(booking);
	}catch(e){
		console.log(e)
	}
});


BookingRouter.get("/showbookings", async (req, res)=>{
	try{
		let bookings = await BookingModel.find();
		res.send(bookings);
	}catch(e){
		console.log(e)
	}
});

// Show all Booked Dates By Class
BookingRouter.get("/showbookingsbyclassid/:id", async (req, res)=>{
	try{
		let bookings = await BookingModel.find({classId:req.params.id});
		res.send(bookings);
	}catch(e){
		console.log(e)
	}
});

// Show all Booked Dates By User
BookingRouter.get("/showbookingsbyuserid/:id", async(req, res)=>{
	try{
		let bookings = await BookingModel.find({userId:req.params.id})
		res.send(bookings)
	}catch(e){
		console.log(e)
	}
})

// Show all booked dates by user and per class
BookingRouter.get("/showbookingsbyuserperclass/:userid/:classid", async(req, res)=>{
	try{
		let bookings = await BookingModel.find({userId:req.params.userid, classId:req.params.classid})
		res.send(bookings)
	}catch(e){
		console.log(e)
	}
})



module.exports = BookingRouter;