const express = require('express');

const ClassesRoute = express.Router();

const ClassesModel = require('../models/Class');

const multer = require('multer');

let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    // 'public/images/uploads' - FILE SAVING DESTINATION
    cb(null, 'public/images/uploads')
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "-" + file.originalname)
  }
})

const upload = multer({storage})

ClassesRoute.post('/addclass', upload.single('image'), async(req, res)=>{
  let imagepath = ""
  // MULTER ENDPOINT
  if(req.file){
    imagepath = "images/uploads/" + req.file.filename
  }else{
    res.status("409").json('No File to Uploads');
  }

  try{
    let newClass = ClassesModel({
      className: req.body.genre,
      instructor: req.body.instructor,
      schedule: req.body.schedule,
      time: req.body.time,
      rate: req.body.rate,
      image: imagepath
    })

    newClass = await newClass.save();
    res.send(newClass);
  }catch(e){
    console.log(e)
  }
})

ClassesRoute.get('/showclasses', async(req, res)=>{
  try{
    let classes = await ClassesModel.find();
    res.send(classes);
  }catch(e){
    console.log(e)
  }
})

ClassesRoute.delete('/deleteclass/:id', async(req, res)=>{
  try{
    let deletedClass = await ClassesModel.findByIdAndDelete(req.params.id)
    res.send(deletedClass)
  }catch(e){
    console.log(e)
  }
})

module.exports = ClassesRoute;
