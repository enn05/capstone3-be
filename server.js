const express = require("express");
const app = express();
const mongoose = require("mongoose");
const config = require("./config");
const cors = require('cors');
const fs = require('fs');

// to connect
const databaseUrl = process.env.DATABASE_URL || "mongodb+srv://admin:batch46092519@b46mongostack-zfef5.mongodb.net/capstone3?retryWrites=true&w=majority";
mongoose
 .connect(
   databaseUrl,
   {
     useNewUrlParser: true,
     useUnifiedTopology: true,
     useFindAndModify: false,
     useCreateIndex: true
   }
 )
 .then(() => {
   console.log("Remote Database Connection Established");
 });

app.use(require("body-parser").text());


//file uploading
const path = require('path');
app.use(express.static(path.join(__dirname, 'public')))

app.use(express.urlencoded({ extended: false }));
// needed for body parsing
app.use(express.json());
app.use(cors());
// set up server
app.listen(config.port, () => {
 console.log(`Listening on Port ${config.port}`);
});

// instructor api
const instructor = require('./routes/instructors_route');
app.use('/', instructor);

// classes api
const classes = require('./routes/classes_routes');
app.use('/', classes);

// use api 
const users = require('./routes/users_router');
app.use('/', users);

// auth api
const auth = require('./routes/auth_router');
app.use('/', auth)

// booking api
const booking = require('./routes/booking_router');
app.use('/', booking)

//*********//
// Stripe //
// ******//
const payment = require('./routes/payment_route');
app.use('/', payment);