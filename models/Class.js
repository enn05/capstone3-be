const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ClassesSchema = new Schema({
  className: String,
  instructor: String,
  schedule: String,
  time: String,
  rate: Number,
  slot: Number,
  image: String
})

module.exports = mongoose.model("Classes", ClassesSchema);