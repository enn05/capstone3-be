const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const InstructorSchema = new Schema({
  name: String,
  email: String,
  genre: String,
})

module.exports = mongoose.model("Instructor", InstructorSchema);