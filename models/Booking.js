const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moment = require('moment');

const BookingSchema = new Schema({
    userName:String,
    userId:String,
    className: String,
    classId: String,
    dates: [String],
    bookingNumber: Number,
    totalPayment:Number,
    status: String,
    transactionDate: {
        type: String,
		default: moment(new Date).format("MM/DD/YYYY")
    },
    transactionCode: String
});
module.exports = mongoose.model("Booking", BookingSchema);